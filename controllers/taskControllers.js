const Task = require('../models/Task');

// getAllTasks
module.exports.getAllTasks = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};

// createTask
module.exports.createTask = (requestBody) => {
  const newTask = new Task({
    name: requestBody.name,
  });
  return newTask.save().then((task, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return task;
    }
  });
};

// deleteTask
module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((removedtask, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return removedtask;
    }
  });
};

// updateTask
module.exports.updateTask = (taskId, newContent) => {
  return Task.findById(taskId).then((result, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      result.name = newContent.name;
      return result.save().then((updatedTask, saveErr) => {
        if (saveErr) {
          console.log(saveErr);
          return false;
        } else {
          return updatedTask;
        }
      });
    }
  });
};

// == ACTIVITY ==

// getTask
module.exports.getTask = (taskId) => {
  return Task.findById(taskId).then((task, err) => {
    if (err) {
      console.log(err);
      return null;
    } else {
      return task;
    }
  });
};

// completeTask
module.exports.completeTask = (taskId) => {
  return Task.findByIdAndUpdate(taskId, { status: 'complete' }).then(
    (task, err) => {
      if (err) {
        console.log(err);
        return null;
      } else {
        console.log(task);
        // findByIdAndUpdate seems to return item's state before the update,
        // query to get new state
        return module.exports.getTask(taskId);
      }
    }
  );
};
