const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes');

const app = express();

const PORT = 3001;

// MongoDB
mongoose.connect(
  'mongodb+srv://admin123:admin123@project0.csykcns.mongodb.net/s36?retryWrites=true&w=majority',
  {
    useNewUrlParser: true, // compatibility
    useUnifiedTopology: true, // compatibility
  }
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('Connected to database'));

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// routes
app.use('/tasks', taskRoutes);

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
